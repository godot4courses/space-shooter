# GalaxyDefiance

## Description

Prototype de Shooter 2D en vue du dessus dans le cadre du tuto [Godot 4 Tutorial - Space Shooter Using Components](https://www.youtube.com/watch?v=zUeLesdL7lE&list=PL9FzW-m48fn09w6j8NowI_pSBVcsb3V78) par Heartbeast.

## Objectifs

Aucun.

## Mouvements et actions du joueur

Déplacer le joueur: touches ZQSD ou bien les flèches du clavier.
Quitter le jeu: clic sur la croix ou touche escape.

## Interactions

Aucun.

## Copyrights

Codé en GDscript en utilisant le moteur de jeu Godot Engine 4.2.
Développé en utilisant principalement l'éditeur de code intégré et/ou Visual Studio Code.

-----------------
(C) GameaMea Studio (https://www.gameamea.com)
